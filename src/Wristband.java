
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
public class Wristband {
    private String Wristband;
    private Date dob;
    private Doctor doctor;
    private Barcode barcode;   
    public Wristband(String Wristband, Date dob, Doctor doctor, Barcode barcode){
        this.Wristband=Wristband;
        this.barcode=barcode;
        this.doctor=doctor;
        this.dob=dob;
    }
    public String getWristband(){
        return this.Wristband;
    }
    public Date getDate(){
        return this.dob;
    }
    public Doctor getDoctor(){
        return this.doctor;
    }
    public Barcode getBarcode(){
        return this.barcode;
    }
    
    
    
    
}

