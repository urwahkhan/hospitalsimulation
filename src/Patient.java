
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
public class Patient extends Person {
	List<Wristband> bands;

	public Patient( String name, List<Wristband> bands ) {
		super( name );
                this.bands=bands;
	}
        public List<Wristband> getbands(){
            return this.bands;
        }
}
