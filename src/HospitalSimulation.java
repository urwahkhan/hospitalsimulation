
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
public class HospitalSimulation {
    public static void main(String[]args){
        Doctor doctor = new Doctor("Dr. Anjum");
        Barcode barcode = new Barcode("893uhbj");
        Medication med = new Medication("Panadol");
        List<Medication> meds = new ArrayList<Medication>();
        meds.add(med);
        String patientName = ("Umamah Patient");
        Wristband band = new AllergyWristBand(patientName, new Date(),doctor,barcode,meds);
        List<Wristband> bands = new ArrayList<Wristband>();
        bands.add(band);
        
        Patient patient = new Patient (patientName, bands);
        List<Patient>patients = new ArrayList<Patient>();
        patients.add(patient);
        
        ResearchGroup group = new ResearchGroup(patients);
        
        
        System.out.println(band);
    }
    
}
